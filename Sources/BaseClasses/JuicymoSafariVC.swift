//
//  JuicymoSafariVC.swift
//  JuicymoKit
//
//  Created by Martin Muller on 23.01.18.
//

import Foundation

import SafariServices

class JuicymoSafariVC: SFSafariViewController {
    private var previousStatusBarStyle = UIStatusBarStyle.default
    
    override init(url URL: URL, entersReaderIfAvailable: Bool) {
        super.init(url: URL, entersReaderIfAvailable: entersReaderIfAvailable)
        
        self.previousStatusBarStyle = UIApplication.shared.statusBarStyle
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIApplication.shared.statusBarStyle = previousStatusBarStyle
    }
}
