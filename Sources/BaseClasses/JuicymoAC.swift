//
//  JuicymoAC.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 28.06.16.
//
//

// Native Frameworks
import Foundation

// Dependencies

open class JuicymoAC: NSObject, JuicymoACP, JuicymoDIRegister {
    
    open let di = JuicymoDI()
    
    open var rootFlow: JuicymoFCP?
    
    open var appDelegate: UIApplicationDelegate?
    
    // MARK: Initialization
    public override init() {
        super.init()
        
        printVersion()
        setupDI(self.di)
        defineModels(self.di)
    }
    
    // MARK: Public Methods
    open func createRootVC<T: JuicymoVCP>() -> T {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadInitialVCinBaseFlow() as! T
        } else {
            preconditionFailure("[JuicymoKit] setupRootFlow() must be called before createRootVC()")
        }
    }
    
    open func createRootVCinNVC() -> UINavigationController {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadBaseFlowWithInitialVC()
        } else {
            preconditionFailure("[JuicymoKit] setupRootFlow() must be called before createRootVCinNVC()")
        }
    }
    
    open func createRootVCasTabBar() -> UITabBarController {
        self.onApplicationStart()
        
        if let flowC = self.rootFlow {
            return flowC.loadInitialTabBarC()
        } else {
            preconditionFailure("[JuicymoKit] setupRootFlow() must be called before createRootVCasTabBar()")
        }
    }
    
    open func changeRootVCasTabBar(with rFlow: JuicymoFCP) {
        self.rootFlow = rFlow
        self.appDelegate?.window??.rootViewController = self.createRootVCasTabBar()
    }
    
    open func changeRootVCinNVC(with rFlow: JuicymoFCP) {
        self.rootFlow = rFlow
        self.appDelegate?.window??.rootViewController = self.createRootVCinNVC()
    }
    
    open func setupRootFlow() {
        createRootFlow(self.di)
    }
    
    open func setupServices() {
        // noop - can be used in subclass
    }
    
    open func defineBindings() {
        // noop - can be used in subclass
    }
    
    open func onApplicationStart() {
        self.defineBindings()
    }
    
    // MARK: Abstract Methods
    open func createRootFlow(_ di: JuicymoDI) {
        preconditionFailure("[JuicymoKit] This method must be overriden by the subclass")
        
        // Example:
        // self.rootFlow = di.resolveBusinessFC()
    }
    
    // MARK: Protected Methods
    open func setupDI(_ di: JuicymoDI) {
        // noop - can be used in subclass
        
        // Example:
        // di.registerBusinessFC { (resolver) -> (BusinessFC) in
        //     BusinessFC(ac: self)
        // }
    }
    
    open func defineModels(_ di: JuicymoDI) {
        // noop - can be used in subclass
        
        // Example:
        // self.model = di.resolveMyAppModel()
    }
    
    open func startNetworkActivityIndication() {
        // noop - can be used in subclass
    }
    
    open func stopNetworkActivityIndication() {
        // noop - can be used in subclass
    }
    
    // MARK: Private Methods
    open func printVersion() {
        print("[JuicymoKit] Version \(JuicymoKitVersionNumber)")
    }
}
