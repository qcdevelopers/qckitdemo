//
//  JuicymoHeaderView.swift
//  JuicymoKit
//
//  Created by Martin Muller on 23.01.18.
//

import UIKit

class JuicymoHeaderView: UIView {
    enum Separator { case Top, Bottom }
    
    private var lblTitle: UILabel!
    
    override init(frame: CGRect) { super.init(frame: frame) }
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    
    convenience init(title: String,
                     separators: [Separator?],
                     height: CGFloat = 66,
                     backgroundColor: UIColor = .white,
                     textColor: UIColor = .black,
                     textFont: UIFont = .systemFont(ofSize: 20),
                     separatorColor: UIColor = UIColor(red:0.78, green:0.78, blue:0.80, alpha:1.00)) {
        
        self.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        lblTitle = UILabel(frame: CGRect(x: 14, y: 0, width: self.frame.width, height: self.frame.height))
        self.addSubview(lblTitle)
        
        self.backgroundColor = backgroundColor
        lblTitle.textColor = textColor
        lblTitle.font = textFont
        
        if !separators.isEmpty {
            if separators.count > 2 { print("Cannot pass more than two separators")
            } else {
                for separator in separators {
                    if separator == .Top { self.addSubview(generateSeparator(y: 0.5, color: separatorColor)) }
                    else if separator == .Bottom { self.addSubview(generateSeparator(y: height, color: separatorColor)) }
                }
            }
        }
    }
    
    private func generateSeparator(y: CGFloat, color: UIColor) -> UIView {
        let separator = UIView(frame: CGRect(x: 0, y: y, width: self.frame.width, height: 0.5))
        separator.backgroundColor = color
        return separator
    }
}
