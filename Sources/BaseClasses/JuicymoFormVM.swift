//
//  JuicymoFormVM.swift
//  Pods
//
//  Created by Tomáš Jukin on 17.09.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import Swinject
import RxSwift
import RxCocoa


// MARK: Public View Model Interface
public protocol JuicymoFormVMP: JuicymoVMP {
    // MARK: Input
    // noop - this will be defined by subclass...
    
    // MARK: Input - Actions
    var actionSave: Driver<Void> { get set }
    
    // MARK: Output
    // noop - validation driver will be defined by subclass...
    //var validation: Driver<ValidationResult<T>> { get }
    
    var statusValidation: Driver<LoadingInfo> { get }
    var errorMessage: Driver<String?> { get }
    var saveEnabled: Driver<Bool> { get }
    var statusLoading: Driver<LoadingInfo> { get }
    
    var result: Driver<Sink> { get } // a default sink result which can be used by subclass
    // noop - more output will be defined by subclass...
    
    // MARK: Methods
}

open class JuicymoFormVM: JuicymoVM, JuicymoFormVMP {
    // MARK: Input - Actions
    open var actionSave: Driver<Void> = Driver.never()
    
    // MARK: Output
    open var statusValidation: Driver<LoadingInfo> = Driver.never()
    open var statusLoading: Driver<LoadingInfo> = Driver.never()
    open var errorMessage: Driver<String?> = Driver.never()
    open var saveEnabled: Driver<Bool> = Driver.never()
    
    open var result: Driver<Sink> = Driver.never() // a default sink result which can be used by subclass
    
    // MARK: Private Pipes
    open var errorMessageRequest: Driver<String?> = Driver.never()
    open var errorMessageValidation: Driver<String?> = Driver.never()
    
    // MARK: Public Methods
    
    // MARK: Protected Methods - Actions Setup
    open func configureActionSave<T>(
        _ action: Driver<Void>, from: Driver<ValidationResult<T>>
        ) -> Driver<T> {
        return action
            .withLatestFrom(from)
            .filter { result -> Bool in
                return result.item != nil
            }
            .map { valResult -> T in
                guard let result = valResult.item else {
                    preconditionFailure("nil is not allowed in action save")
                }
                
                return result
        }
    }
    
    open func combine(actionA: Driver<Void>, actionB: Driver<Void>) -> Driver<Void> {
        return Driver.of(actionA, actionB).merge()
    }
    
    // MARK: Protected Methods - Drivers Setup
    open func configureResult<T>(_ result: Driver<PipeResult<T>>) -> Driver<T> {
        return result
            .filter { pipeResult in pipeResult.item != nil }
            .map { pipeResult in
                guard let item = pipeResult.item else { preconditionFailure("Item cannot be nil here") }
                
                return item
        }
    }
    
    open func configureStatusValidation<T>(_ validator: Driver<ValidationResult<T>>)
        -> Driver<LoadingInfo> {
            return validator
                .map { result in
                    if result.ok == .success {
                        return .success
                    } else if result.ok == .failure {
                        return .failure
                    } else if result.ok == .notYet {
                        return .notYet
                    } else {
                        return .inProgress
                    }
            }
    }
    
    open func configureErrorMessageValidation<T>(_ validator: Driver<ValidationResult<T>>)
        -> Driver<String?> {
            return validator
                .filter { result in
                    return result.ok == .failure || result.ok == .success
                }
                .map { result -> String? in
                    if let message = result.message {
                        return result.ok == .success ? nil : message
                    } else {
                        return nil
                    }
            }
    }
    
    open func configureErrorMessage(
        _ statusValidation: Driver<LoadingInfo>, msgValidation: Driver<String?>, msgRequest: Driver<String?>
        ) -> Driver<String?> {
        return Driver.combineLatest(statusValidation, msgValidation, msgRequest) { status, msgValidation, msgRequest in
            return (status, msgValidation, msgRequest)
            }
            .map { (status: LoadingInfo, msgValidation: String?, msgRequest: String?) -> String? in
                return status == .success ? msgRequest : msgValidation
            }
            .distinctUntilChanged({ (lhs, rhs) -> Bool in
                if lhs == nil && rhs == nil {
                    return true
                } else if lhs != nil && rhs != nil && lhs == rhs {
                    return true
                } else {
                    return false
                }
            })
    }
    
    open func configureSaveEnabled(_ validation: Driver<LoadingInfo>, loading: Driver<LoadingInfo>) -> Driver<Bool> {
        return (validation + loading)
            .map { resultValidation, resultLoading in
                return resultValidation == .success && resultLoading != .inProgress
        }
    }
    
    open func configureStatusLoading<T>(_ result: Driver<PipeResult<T>>) -> Driver<LoadingInfo> {
        return result
            .map { result in
                return result.ok
        }
    }
    
    open func configureErrorMessageRequest<T>(_ result: Driver<PipeResult<T>>) -> Driver<String?> {
        return result
            .map { pipeResult -> String? in
                guard let message = pipeResult.message else { return nil }
                
                return pipeResult.ok == .inProgress ? nil : message
        }
    }
}
