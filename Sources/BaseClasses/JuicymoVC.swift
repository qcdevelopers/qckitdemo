//
//  JuicymoVC.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

// Native Frameworks
import Foundation
import UIKit

// Dependencies
import Swinject
import RxSwift
import RxCocoa
import Alamofire
import SwiftyJSON
import NSObject_Rx
import Sugar

open class JuicymoVC: UIViewController {
    open var traceLifecycle = false
    open var traceSetup = false
    open var traceRefresh = true
    open var traceBinding = false
    
    var di: JuicymoDI?
    var alertView: UIView? = nil
    var noReviewsView: UIView? = nil
    var notificationView: JuicymoNotificationView? = nil
    
    // Protected
    open var viewModel: JuicymoVMP?
    open var navbarProgressColor: UIColor?
    
    // MARK: Initialization
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setupInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupInit()
    }
    
    // MARK: Setup
    fileprivate func setupInit() {
        // noop
    }
    
    open func defineViewModel(_ di: JuicymoDI) -> JuicymoVMP {
        preconditionFailure("[JuicymoKit] This method must be overriden by the subclass")
        
        // Example:
        // return di.resolveBusinessMapVM()
    }
    
    // MARK: View Lifecycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        setupPermissions()
        setupDelegates()
        setupUI()
        
        interConnectLocalUi()
        
        if var vm = self.viewModel {
            vm.active = false
            bindRefreshToViewModel()
            vm.prepareOutputBindings()
            bindToViewModel(vm)
            vm.processInputBindings()
        }
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        updateUI()
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        if var vm = self.viewModel {
            vm.active = true
            
            vm.refreshIfNeeded()
        }
        
        fillUI()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        if var vm = self.viewModel {
            vm.active = false
            
            vm.cleanUp()
        }
    }
    
    // MARK: Protected Methods
    open func setupPermissions() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func setupDelegates() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func setupUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    /// Warning: A protected method, do not call directly!
    open func updateUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    /// Warning: A protected method, do not call directly!
    open func fillUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func bindRefreshToViewModel() {
        if traceBinding { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
        
        // Example:
        // self.btnRefresh ==> bvm.actionRefreshDefault
    }
    
    /**
     Performs binding of View Controller UI elements and events to provided View Model.
     
     - Warning: A protected method, do not call directly!
     
     - Note: You should use this method to define binding of your View Controller's UI elements
     to View Model's Variables and Drivers
     
     - Parameters:
       - vm: The View Model used for binding
     */
    open func bindToViewModel(_ vm: JuicymoVMP) {
        if traceBinding { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func interConnectLocalUi() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    // MARK: Private Methods
    fileprivate func setupViewModel() {
        guard let di = self.di else {
            preconditionFailure("[JuicymoKit] DI container must be initialized before View Model can be configured. Call connectToDIContainer() before presenting this VC")
        }
        
        self.viewModel = defineViewModel(di)
    }
    
    // MARK: Multiline Navigation Bar title
    open func createMultiLineLabel(_ firstLine: String, secondLine: String, firstLineColor: UIColor, secondLineColor: UIColor) -> UIView {
        let view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 180, height: 44))
        view.backgroundColor = UIColor.clear
        let firstLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 180, height: 25))
        let secondLabel = UILabel(frame: CGRect(x: 0, y: 18, width: 180, height: 25))
        if #available(iOS 8.2, *) {
            firstLabel.font = UIFont.systemFont(ofSize: 17 ,weight: UIFont.Weight.medium)
            secondLabel.font = UIFont.systemFont(ofSize: 17 ,weight: UIFont.Weight.medium)
        } else {
            // Fallback on earlier versions
        }
        firstLabel.text = firstLine
        firstLabel.textAlignment = .center
        firstLabel.textColor = firstLineColor
        secondLabel.textColor = secondLineColor
        secondLabel.textAlignment = .center
        secondLabel.text = secondLine
        view.addSubview(firstLabel)
        view.addSubview(secondLabel)
        
        return view
    }
    
    open func showActionSheet(cancel: Bool = false, actionSheetTitle: String? = nil, actionSheetMessage: String? = nil, actionStyle: UIAlertActionStyle = .default, cancelStyle: UIAlertActionStyle = .default, elements: [String], _ completion: @escaping (_ result: String) -> Void) {
        let actionSheetController: UIAlertController = UIAlertController(title: actionSheetTitle, message: actionSheetMessage, preferredStyle: .actionSheet)
        
        var actionButton: UIAlertAction = UIAlertAction()
        
        for element in elements {
            actionButton = UIAlertAction(title: element, style: actionStyle)
            { action -> Void in
                completion(element)
            }
            actionSheetController.addAction(actionButton)
        }
        
        if cancel {
            let cancelButton = UIAlertAction(title: "Cancel", style: cancelStyle)
            { action -> Void in
                completion("Cancel")
            }
            
            actionSheetController.addAction(cancelButton)
        }
//        actionSheetController.view.tintColor = UIColor.red
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

// MARK: UI & MVVM
extension JuicymoVC: JuicymoVCP {
    // MARK: Public Methods
    public func connectToDIContainer(_ di: JuicymoDI) {
        self.di = di
        
        setupViewModel()
    }
}

// MARK: Keyboard Observation
extension JuicymoVC {
    @objc public func keyboardWillShow(_ n: Notification) { /* noop - can be used in subclass */ }
    
    @objc public func keyboardWillHide(_ n: Notification) { /* noop - can be used in subclass */ }
    
    public func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    public func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
