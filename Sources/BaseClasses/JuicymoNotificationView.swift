//
//  JuicymoNotificationView.swift
//  JuicymoKit
//
//  Created by Martin Muller on 23.01.18.
//

import UIKit

import RxSwift
import RxCocoa

class JuicymoNotificationView {
    // MARK: Variables
    private var view: UIView!
    private var lblTitle: UILabel!
    private var timer: Timer?
    
    private var parentVC: UIViewController?
    
    // MARK: Callbacks
    var onDidAppear: () -> () = { }
    var onDidDisappear: () -> () = { }
    
    // MARK: Initializer
    init(on vc: UIViewController, height: CGFloat = 32) {
        parentVC = vc
        
        view = UIView(frame: CGRect(x: 0, y: -100, width: vc.view.frame.width, height: height))
        view.backgroundColor = .white
        
        lblTitle = UILabel(frame: desiredLblFrame)
        lblTitle.textAlignment = .center
        lblTitle.font = UIFont.systemFont(ofSize: 14)
        
        view.addSubview(lblTitle)
        vc.view.addSubview(view)
        
        handleRxTitle()
    }
    
    // MARK: Public computed variables (API)
    public var height: CGFloat {
        get { return view.frame.height }
        set { set(height: height) }
    }
    
    public var width: CGFloat { return parentVC?.view.frame.width ?? UIScreen.main.bounds.width }
    public var backgroundColor: UIColor? {
        get { return view.backgroundColor }
        set { set(backgroundColor: newValue) }
    }
    
    public var textColor: UIColor {
        get { return lblTitle.textColor }
        set { set(textColor: newValue) }
    }
    
    public var textFont: UIFont {
        get { return lblTitle.font }
        set { set(textFont: newValue) }
    }
    
    public var title: String? {
        get { return lblTitle.text }
        set { set(title: newValue) }
    }
    
    // MARK: Public variables (API)
    public var useContrastingTitleColor: Bool = true
    public var rxTitle: Driver<String?> = Driver.never()
    
    // MARK: Private computed variables
    private var desiredLblFrame: CGRect { return CGRect(x: 10, y: (height - 14) / 2, width: width - 20, height: 14) }
    private var desiredFullViewY: CGFloat {
        if parentVCHasNavigationBar {
            if #available(iOS 11.0, *) { return parentVC?.navigationController?.navigationBar.frame.maxY ?? parentVC?.view.safeAreaInsets.top ?? 64 }
            else { return 64 }
        } else {
            if #available(iOS 11.0, *) { return parentVC?.view.safeAreaInsets.top ?? 0 }
            else { return 0 }
        }
    }
    
    private var parentVCHasNavigationBar: Bool {
        if parentVC?.navigationController != nil && parentVC?.navigationController?.isNavigationBarHidden == false { return true }
        return false
    }
    
    private var isVisible: Bool {
        if view.frame.origin.y < -height { return false }
        return true
    }
    
    // MARK: Private methods
    private func handleRxTitle() { rxTitle.drive(onNext: { (title) in self.set(title: title) }).disposed(by: DisposeBag()) }
    
    private func set(height: CGFloat) {
        if height >= 14 {
            UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .allowAnimatedContent, animations: {
                self.view.frame = CGRect(x: 0, y: self.view.frame.origin.y, width: self.width, height: height)
                self.lblTitle.frame = self.desiredLblFrame
            }, completion: { (_) in })
        }
    }
    
    private func set(backgroundColor: UIColor?) {
        UIView.animate(withDuration: 0.2) {
            if self.useContrastingTitleColor && backgroundColor != nil { self.lblTitle.textColor = backgroundColor!.contrastingTitleColor() }
            self.view.backgroundColor = backgroundColor
        }
    }
    
    private func set(textColor: UIColor) {
        useContrastingTitleColor = false
        lblTitle.textColor = textColor
    }
    
    private func set(textFont: UIFont) { lblTitle.font = textFont }
    
    private func set(title: String?) {
        if title != nil {
            if timer != nil {
                timer!.invalidate()
                timer = nil }
            
            lblTitle.text = title
            
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
            
            show()
        }
    }
    
    @objc private func hide() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .allowAnimatedContent, animations: {
            self.view.frame.origin.y = -self.height
        }, completion: { (_) in
            self.lblTitle.text = ""
            self.onDidDisappear()
        })
    }
    
    private func show() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .allowAnimatedContent, animations: {
            self.view.frame.origin.y = self.desiredFullViewY
        }, completion: { (_) in self.onDidAppear() })
    }
}
