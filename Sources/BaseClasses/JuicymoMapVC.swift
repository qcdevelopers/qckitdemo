//
//  JuicymoMapVC.swift
//  JuicymoKit
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

// Native Frameworks
import Foundation
import MapKit

open class JuicymoMapVC: JuicymoVC/*, CLLocationManagerDelegate*/ {
    //public let locationManager = CLLocationManager() // TODO remove this, it is in model
    
    // MARK: Protected Methods
    override open func setupDelegates() {
        super.setupDelegates()
        
        //setupLocationManager()
    }
    
    override open func setupPermissions() {
        super.setupPermissions()
        
        //locationManager.requestWhenInUseAuthorization() // TODO remove this
    }
    
    // MARK: Private Methods
    //    private func setupLocationManager() { // TODO remove this
    //        //locationManager.delegate = self
    //    }
}
