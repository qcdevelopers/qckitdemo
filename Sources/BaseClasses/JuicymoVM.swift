//
//  JuicymoVM.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import Swinject
import RxSwift
import RxCocoa
import NSObject_Rx


/**
 Loading status information.
 
 - InProgress: Loading in progress.
 - Success: Loading finishled successfully.
 - Failure: There was a failure while loading.
 - NotYet: There is no result yet.
 */
public enum LoadingInfo {
    case inProgress, success, failure, notYet
}

/**
 Validation status information.
 
 - InProgress: Validation in progress.
 - Success: Validation finishled successfully.
 - Failure: There was a failure in validation.
 - NotYet: There is no result yet.
 */
public enum ValidationInfo {
    case inProgress, success, failure, notYet
}

// TODO rename to Result<T>
public struct ValidationResult<T> {
    public let ok: ValidationInfo
    public let message: String?
    public let item: T?
}

open class JuicymoVM: NSObject, JuicymoVMP {
    /// Is this VM active? (should is forward data)
    public var active: Bool

    // MARK: Debug Tracing
    open var traceSetup = false
    open var traceRefresh = false
    
    open var actionRefreshDefault: Driver<Void> = Driver.never()
    open var triggerRefreshDefault: Variable<Bool> = Variable(false) // by default no refresh is performed
    
    // MARK: Protected Properties
    open var models: [String: JuicymoModelP] = [:]
    open let di: JuicymoDI
    
    open let minimalIntervalRequest       = 1.0
    open let minimalIntervalErrorMessage  = 1.0
    open let minimalIntervalValidation    = 0.2
    
    // MARK: Private Properties
    fileprivate var isValid = true
    
    fileprivate var skipInputProcessingByModelA: Bool = false
    fileprivate var skipInputProcessingByModelB: Bool = false
    
    // MARK: Initialization
    public init(diProvider: JuicymoDIProvider) {
        self.di = diProvider.di
        
        // Not sure if correct way
        self.active = true
        super.init(  )
        
        setupDI(self.di)
        setupModels()
    }
    
    // MARK: Setup
    open func defineModels(_ di: JuicymoDI) {
        preconditionFailure("[JuicymoKit] Method defineModels() must be overriden by the class \(String(describing: type(of: self)))")
        
        // Example:
        // models["myModel"] = di.resolveMyModel()
    }
    
    // MARK: Public Methods
    open func invalidate() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        self.isValid = false
    }
    
    open func refreshIfNeeded() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        if self.isValid == false {
            refresh()
        }
    }
    
    open func refresh() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        performRefresh()
        
        self.isValid = true
    }
    
    /**
     When your VM sets anything in model (a Variable value or Driver of the model)
     you should release those resources in this method.
    
     Otherwise they will be triggered again everytime some other VM will use the model
     - because model instaces are reused in JuicymoKit MVVM apps.
    
     - Note: In either case you need to override this method in your VM.
     - Note: VM actions or forwarded drivers do not need to be cleaned
     
     - Warning: Never call this method directly, it is called automatically by JuicymoVC in its lifecycle.
     
     - Experiment: Example usage when VM does use Model input
         // Clean up local Variables
         self.inputNewStaffMemberName.value = nil
         
         // Ask model to clear related requests
         modelAddStaffMember().clearRequestAddStaffMember()
     
     - Experiment: Example usage when VM does not pass data to Model
         // noop, there is no need for clean up, this VM does not send data to Model
     */
    open func cleanUp() {
        preconditionFailure("[JuicymoKit] Method cleanUp() must be overriden by the class \(String(describing: type(of: self)))")
    }
    
    open func prepareOutputBindings() {
        if self.models.count > 0 {
            bindRefreshToModels()
            
            if traceSetup { traceClass(String(describing: type(of: self))) }
            bindOutputToModels(self.models)
        }
    }
    
    open func processInputBindings() {
        if self.models.count > 0 {
            bindActionRefresh();
            
            if traceSetup { traceClass(String(describing: type(of: self))) }
            bindInputToModels(self.models)
            
            if self.skipInputProcessingByModelA == false && self.skipInputProcessingByModelB == false {
                if traceSetup { traceClass(String(describing: type(of: self))) }
                
                // Let every model bind the input pipes (set in bindToModels)
                self.models.forEach({ _, model in
                    model.processInputBindings()
                })
            }
        }
    }
    
    // MARK: Protected Methods - Setup
    open func setupDI(_ di: JuicymoDI) {
        // noop - can be used in subclass
    }
    
    open func bindOutputToModels(_ models: [String: JuicymoModelP]) {
        // noop - can be used in subclass
        
        // subclass should override this method and it SHOULD NOT call super.bindOutputToModels!
        
        // this code will prevent the model processInputBindings() to be called when not used by this VM
        self.skipInputProcessingByModelA = true
    }
    
    open func bindInputToModels(_ models: [String: JuicymoModelP]) {
        // noop - can be used in subclass
        
        // subclass should override this method and it SHOULD NOT call super.bindOutputToModels!
        
        // this code will prevent the model processInputBindings() to be called when not used by this VM
        self.skipInputProcessingByModelB = true
    }
    
    // MARK: Protected Methods - Refreshing
    open func performRefresh() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        self.models.forEach({ _, model in
            model.performRefreshDefault()
        })
    }
    
    // MARK: Protected Methods - Throttling
    open func throttle<T>(_ binding: Driver<T>) -> Observable<T> {
        print("[JuicymoKit] DEPRECATED: View Model \(String(describing: type(of: self))) is using deprecated method throttle<T>")
        return binding.asObservable()
    }
    
    // MARK: Private Methods - Result factories-
    open func resultSuccess<T>(_ item: T) -> ValidationResult<T> {
        return ValidationResult(ok: .success, message: nil, item: item)
    }
    
    open func resultFailed<T>(_ message: String) -> ValidationResult<T> {
        return ValidationResult(ok: .failure, message: message, item: nil)
    }
    
    open func resultInProgress<T>() -> ValidationResult<T> {
        return ValidationResult(ok: .inProgress, message: "Loading...", item: nil)
    }
    
    open func resultNotYet<T>() -> ValidationResult<T> {
        return ValidationResult(ok: .notYet, message: nil, item: nil)
    }
    
    // MARK: Private Methods
    open func modelNotFound() -> String {
        return "[JuicymoKit] Model for VM \(String(describing: type(of: self))) is not defined, I cannot bind it"
    }
    
    func bindActionRefresh() {
        let refreshDriverRx = configureActionRefresh(self.actionRefreshDefault, from: Driver.just(true))
        let refreshDriverNonRx = configureTriggerRefresh(self.triggerRefreshDefault)
        
        (Driver.of(refreshDriverRx, refreshDriverNonRx).merge() --> { _ in self.refresh() }) ~> rx_disposeBag
    }
    
    func setupModels() {
        defineModels(self.di)
    }
    
    func bindRefreshToModels() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        self.models.forEach({ _, model in
            var mutableModel = model
            
            self.actionRefreshDefault ==> mutableModel.actionRefreshDefault
            model.prepareOutputBindings() // let model prepare bindings
        })
    }
    
    // MARK: Private Methods - Actions Setup
    fileprivate func configureActionRefresh(_ action: Driver<Void>, from: Driver<Bool>
        ) -> Driver<Bool> {
        return action.withLatestFrom(from)
    }
    
    // MARK: Private Methods - Triggers Setup
    fileprivate func configureTriggerRefresh(_ trigger: Variable<Bool>) -> Driver<Bool> {
        return trigger.asDriver()
            .filter { trigger -> Bool in
                return trigger == true
        }
    }
}

// Juicymo Rx Operators
infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
