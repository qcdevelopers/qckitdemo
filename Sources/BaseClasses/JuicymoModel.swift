//
//  JuicymoModel.swift
//  Pods
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import Moya
import SwiftyJSON
import RxSwift
import RxCocoa
import NSObject_Rx

public typealias JuicymoWebErrorHandler = (_ statusCode: Int, _ response: JSON) -> Void

/**
 Juicymo Model mode.
 
 - Hot: Fetches data as soon as possible (automatically).
 - Cold: Data fetch has to be invoked.
 */
public enum JuicymoModelMode {
    case hot, cold
}

public struct PipeResult<T>: CustomStringConvertible, CustomDebugStringConvertible {
    public let ok: LoadingInfo
    public let message: String?
    public let item: T?
    
    public init(ok: LoadingInfo, message: String?, item: T?) {
        self.ok = ok
        self.message = message
        self.item = item
    }
    
    public var description: String {
        return "\(self.ok): \(String(describing: self.message)) - \(String(describing: self.item))"
    }
    
    public var debugDescription: String {
        return self.description
    }
}

open class JuicymoModel: NSObject, JuicymoModelP {
    open var traceSetup = false
    open var traceRefresh = false
    open var traceBinding = false
    
    open let di: JuicymoDI
    open var networkActivityClosure: NetworkActivityPlugin.NetworkActivityClosure?
    
    fileprivate var isBoundRefreshDefault = false
    open var bindingRefreshDefault: Driver<Bool> = Driver.never()
    
    open var actionRefreshDefault: Driver<Void> = Driver.never()
    open var triggerRefreshDefault: Variable<Bool> = Variable(false)
    
    public init(diProvider: JuicymoDIProvider) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        self.di = diProvider.di
        
        super.init()
        
        setupDI(self.di)
        setupChildModels(self.di)
        
        defineServices(self.di)
    }
    
    open func prepareOutputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        bindActionRefresh()
        defineOutputBindings()
    }
    
    // MARK: Setup Methods
    open func setupDI(_ di: JuicymoDI) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Example:
        // di.registerGeolocationService { (resolver) -> (GeolocationService) in
        //     GeolocationService()
        // }
    }
    
    open func setupChildModels(_ di: JuicymoDI) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Example:
        // di.registerBusinessModel { (resolver) -> (BusinessModel) in
        //     BusinessModel(diProvider: self)
        // }
    }
    
    // MARK: Define Methods - Services
    open func defineServices(_ di: JuicymoDI) {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        self.createApiService()
        
        // noop - can be used in subclass
        
        // Example:
        // self.geolocationService = di.resolveGeolocationService()
    }
    
    // MARK: Define Methods - API
    open func defineApiService() {
        preconditionFailure("[JuicymoKit] Method defineApiService() must be overriden by the class \(String(describing: type(of: self)))")
        
        // Example:
        // self.serviceAPI = RxMoyaProvider<UrateUApiService>(
        //   plugins: networkPlugins(debug: false, debugResponseData: false),
        //   endpointClosure: endpointClosure(Defaults[.accessToken])
        // )
    }
    
    open func isApiDefined() -> Bool {
        preconditionFailure("[JuicymoKit] Method isApiDefined() must be overriden by the class \(String(describing: type(of: self)))")
        
        // Example:
        // return self.serviceAPI != nil
    }
    
    // MARK: Define Methods - API
    open func defineDefaultMode() -> JuicymoModelMode {
        // noop - can be changed in subclass
        
        return .hot
    }
    
    // MARK: Define Methods - Bindings
    open func defineOutputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Here you can define Rx Model bindings when you need to define them in Model, instead of in ViewModel which is the default
    }
    
    open func defineInputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
        
        // Here you can define Rx Model bindings for input items (= for drivers which are SET after model is setup)
        // Proper call of this method is ensured by calling processInputBindings() from VM
        // Which should be done after bindToModels() of VC execution is ended
        // The needed call to processInputBindings() is done automatically by JuicymoVM class
    }
    
    // MARK: Public Methods (called from VM on refresh)
    open func performRefreshDefault() {
        if traceRefresh { traceClass(String(describing: type(of: self))) }
        
        // noop - can be used in subclass
    }
    
    // MARK: Protected Methods
    open func bindActionRefresh() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        bindActionRefreshDefault()
        
        // noop - can be used in subclass and mode refresh action can be add here...
    }
    
    open func bindActionRefreshDefault() {
        if !isBoundRefreshDefault {
            if traceSetup { traceClass(String(describing: type(of: self))) }
            
            let mode = self.defineDefaultMode()
            if traceBinding {
                print("[JuicymoKit] Model \(String(describing: type(of: self))): Action \"Default\" is initialized as \((mode == .hot ? "HOT" : "COLD"))")
            }
            self.triggerRefreshDefault.value = (mode == .hot ? true : false)
            
            let refreshDriverRx = configureActionRefresh(self.actionRefreshDefault, from: Driver.just(true))
            let refreshDriverNonRx = configureTriggerRefresh(self.triggerRefreshDefault)
            
            self.bindingRefreshDefault = Driver.of(refreshDriverRx, refreshDriverNonRx).merge()
            (self.bindingRefreshDefault --> { _ in self.performRefreshDefault() }) ~> rx_disposeBag
            
            self.isBoundRefreshDefault = true
        } else {
            if traceBinding {
                print("[JuicymoKit] Model \(String(describing: type(of: self))): Action \"Default\" is already bound")
            }
        }
    }
    
    open func createApiService(_ forceReload: Bool = false) {
        if forceReload == true || !isApiDefined() {
            defineApiService()
        }
    }
    
    open func reloadApiService() {
        createApiService(true)
    }
    
    open func processInputBindings() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        
        defineInputBindings()
    }
    
    open func apiNotFound() -> String {
        return "[JuicymoKit] API is not defined, I cannot bind it"
    }
    
    open func serviceNotFound(_ name: String) -> String {
        return "[JuicymoKit] Service \(name) is not defined, I cannot bind it"
    }
    
    open func handleError(_ rawError: Swift.Error, action: JuicymoWebErrorHandler) -> String {
        let error = rawError as! Moya.MoyaError // swiftlint:disable:this force_cast
        
        let message: String
        var statusCode: Int = 0
        if let response = error.response {
            statusCode = response.statusCode
            do {
                let rawJson = try response.mapJSON()
                let json = JSON(rawJson)
                
                action(response.statusCode, json)
                
                if let errorMessage = json["error"].string {
                    message = errorMessage
                } else {
                    if let errors = json["error"].dictionary {
                        if errors.count > 0 {
                            if let errorTuple = errors.first {
                                let (field, reasonOpt) = errorTuple
                                if let reason = reasonOpt.string {
                                    message = "\(field.capitalized) \(reason)"
                                } else {
                                    message = "\(field.capitalized) is invalid"
                                }
                            } else {
                                message = "[JuicymoKit] Response JSON 'error' key first item is empty"
                            }
                        } else {
                            message = "[JuicymoKit] Response JSON 'error' key does not contain any errors"
                        }
                    } else {
                        message = "[JuicymoKit] Response JSON does not contain 'error' key"
                    }
                }
            } catch {
                message = "[JuicymoKit] Response \(response.statusCode) from server does not contain JSON"
            }
        } else {
            message = "[JuicymoKit] Loading error does not contain Response"
        }
        
        print("[JuicymoKit] \(String(describing: type(of: self))): Network Error [\(statusCode)] \(message)")
        
        return message
    }
    
    open func networkPlugins(debug: Bool = false, debugResponseData: Bool = false) -> [PluginType] {
        var plugins: [PluginType] = []
        
        if debug == true {
            plugins.append(NetworkLoggerPlugin(verbose: debugResponseData, responseDataFormatter: JuicymoJSONDebugRDF))
        }
        
        if let activityClosure = self.networkActivityClosure {
            plugins.append(NetworkActivityPlugin(networkActivityClosure: activityClosure))
        }
        
        return plugins
    }
    
    // MARK: Private Methods - Actions Setup
    open func configureActionRefresh(_ action: Driver<Void>, from: Driver<Bool>
        ) -> Driver<Bool> {
        return action.withLatestFrom(from)
    }
    
    // MARK: Private Methods - Triggers Setup
    open func configureTriggerRefresh(_ trigger: Variable<Bool>) -> Driver<Bool> {
        return trigger.asDriver()
            .filter { trigger -> Bool in
                return trigger == true
        }
    }
    
    // MARK: Private Methods - Result factories
    open func resultSuccess<T>(_ message: String, item: T?) -> PipeResult<T> {
        return PipeResult(ok: .success, message: message, item: item)
    }
    
    open func resultFailed<T>(_ message: String) -> PipeResult<T> {
        return PipeResult(ok: .failure, message: message, item: nil)
    }
    
    open func resultInProgress<T>() -> PipeResult<T> {
        return PipeResult(ok: .inProgress, message: "Loading...".localized, item: nil)
    }
    
    open func resultNotYet<T>() -> PipeResult<T> {
        return PipeResult(ok: .notYet, message: nil, item: nil)
    }
}

// Juicymo Rx Operators
infix operator -->; infix operator ==>; infix operator ~>; infix operator <->; infix operator +
