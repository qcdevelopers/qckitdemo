//
//  JuicymoTBC.swift
//  Pods
//
//  Created by Martin Muller on 14.06.17.
//
//

// Native Frameworks
import Foundation
import UIKit

// Dependencies
import Swinject
import RxSwift
import RxCocoa
import Alamofire
import SwiftyJSON
import NSObject_Rx
import Sugar

open class JuicymoTBC: UITabBarController {
    open var traceLifecycle = false
    open var traceSetup = false
    open var traceRefresh = true
    open var traceBinding = false
    
    var di: JuicymoDI?
    var alertView: UIView? = nil
    var noReviewsView: UIView? = nil
    
    // Protected
    open var viewModel: JuicymoVMP?
    open var navbarProgressColor: UIColor?
    
    // MARK: Initialization
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setupInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupInit()
    }
    
    // MARK: Setup
    fileprivate func setupInit() {
        // noop
    }
    
    // MARK: View Lifecycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        setupPermissions()
        setupDelegates()
        setupUI()
        
        interConnectLocalUi()
        
        if var vm = self.viewModel {
            vm.active = false
            bindRefreshToViewModel()
            vm.prepareOutputBindings()
            bindToViewModel(vm)
            vm.processInputBindings()
        }
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        updateUI()
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        if var vm = self.viewModel {
            vm.active = true
            
            vm.refreshIfNeeded()
        }
        
        fillUI()
    }
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
    }
    
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if traceLifecycle { traceClass(String(describing: type(of: self))) }
        
        if var vm = self.viewModel {
            vm.active = false
            
            vm.cleanUp()
        }
    }
    
    // MARK: Protected Methods
    open func setupPermissions() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func setupDelegates() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func setupUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    /// Warning: A protected method, do not call directly!
    open func updateUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    /// Warning: A protected method, do not call directly!
    open func fillUI() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func bindRefreshToViewModel() {
        if traceBinding { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
        
        // Example:
        // self.btnRefresh ==> bvm.actionRefreshDefault
    }
    
    /**
     Performs binding of View Controller UI elements and events to provided View Model.
     
     - Warning: A protected method, do not call directly!
     
     - Note: You should use this method to define binding of your View Controller's UI elements
     to View Model's Variables and Drivers
     
     - Parameters:
     - vm: The View Model used for binding
     */
    open func bindToViewModel(_ vm: JuicymoVMP) {
        if traceBinding { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
    
    open func interConnectLocalUi() {
        if traceSetup { traceClass(String(describing: type(of: self))) }
        // noop - can be used in subclass
    }
}

// MARK: UI & MVVM
extension JuicymoTBC: JuicymoVCP {
    // MARK: Public Methods
    public func connectToDIContainer(_ di: JuicymoDI) {
        self.di = di
        

    }
}

// MARK: Keyboard Observation
extension JuicymoTBC {
    @objc public func keyboardWillShow(_ n: Notification) { /* noop - can be used in subclass */ }
    
    @objc public func keyboardWillHide(_ n: Notification) { /* noop - can be used in subclass */ }
    
    public func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    public func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}
