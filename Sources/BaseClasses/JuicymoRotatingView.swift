//
//  JuicymoRotatingView.swift
//  JuicymoKit
//
//  Created by Martin Muller on 23.01.18.
//

import UIKit

class JuicymoRotatingView: UIView {
    private var lblTitle: UILabel!
    
    override init(frame: CGRect = CGRect(x: 0, y: 22, width: 180, height: 22)) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupUI()
    }
    
    private func setupUI() {
        lblTitle = UILabel(frame: self.frame)
        lblTitle.textAlignment = .center
        
        self.addSubview(lblTitle)
        
        self.backgroundColor = .clear
    }
    
    public var textFont: UIFont {
        get { return lblTitle.font }
        set { lblTitle.font = newValue }
    }
    
    public var textColor: UIColor? {
        get { return lblTitle.textColor }
        set { lblTitle.textColor = newValue }
    }
    
    public var text: String? {
        get { return lblTitle.text }
        set {
            lblTitle.pushTransition(duration: 0.2)
            lblTitle.text = newValue
        }
    }
}
