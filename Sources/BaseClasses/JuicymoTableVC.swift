//
//  JuicymoTableVC.swift
//  Pods
//
//  Created by Martin Muller on 26.07.16.
//
//

// Native Frameworks
import Foundation
import UIKit

open class JuicymoTableVC: JuicymoVC {
    @IBOutlet open weak var tableView: UITableView!
    
    open var defaultItems: [String] = []
    open var defaultCellIdentifier = "DefaultCell"
    
    // MARK: Protected Methods
    override open func setupDelegates() {
        super.setupDelegates()
        
        setupTableView()
    }
    
    // MARK: Private Methods
    fileprivate func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension JuicymoTableVC: UITableViewDelegate {
    
}

extension JuicymoTableVC: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return defaultItems.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: defaultCellIdentifier, for: indexPath)
        
        self.configureCell(cell, forIndexPath: indexPath)
        
        return cell
    }
    
    public func configureCell(_ cell: UITableViewCell, forIndexPath indexPath: IndexPath) {
        // noop - can be used in subclass
    }
}
