//
//  Request.swift
//  Pods
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


extension Alamofire.DataRequest {
    public func responseSwiftyJSON(_ completionHandler: @escaping (DataResponse<JSON>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<JSON> { request, response, data, error in
            guard error == nil else { return .failure(error!) }

            let JSONResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = JSONResponseSerializer.serializeResponse(request, response, data, error)
            
            switch result {
            case .success(let value):
                return .success(JSON(value))
            case .failure(let error):
                return .failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    public func responseObject<T: ResponseObjectSerializable>(_ completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<T> { request, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            let JSONResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = JSONResponseSerializer.serializeResponse(request, response, data, error)
            
            switch result {
            case .success(let value):
                if let _ = response {
                    return .success(T.object(JSON(value)))
                }
                else {
                    _ = "Response object could not be deserialized from JSON, dump is: \(value)"
                    // Temporary
                    let error = AFError.responseSerializationFailed(reason: AFError.ResponseSerializationFailureReason.inputDataNil)
                    return .failure(error)
                }
            case .failure(let error):
                return .failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    public func responseCollection<T: ResponseCollectionSerializable>(_ completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        let responseSerializer = DataResponseSerializer<[T]> { request, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            let JSONSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
            let result = JSONSerializer.serializeResponse(request, response, data, error)
            
            switch result {
            case .success(let value):
                if let _ = response {
                    return .success(T.collection(JSON(value)))
                }
                else {
                    _ = "Response collection could not be deserialized from JSON` due to a nil response"
                    // http://www.swiftbyexamples.com/2016/05/12/alamofire-custom-nserror-implementation-to-avoid-using-deprecated-errorwithcode-_-failurereason/
                    // errorWithCode is deprecated, use custom enum as desribed above
                    // Temporary
                    let error = AFError.responseSerializationFailed(reason: AFError.ResponseSerializationFailureReason.inputDataNil)
                    return .failure(error)
                }
            case .failure(let error):
                return .failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}
