//
//  DoubleExtensions.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

extension Double {
    public func format(_ f: String) -> String { return NSString(format: "%\(f)f" as NSString, self) as String }
    public func round(_ i: Int) -> Double { return NSString(format: "%0.\(i)f" as NSString, self).doubleValue }
    
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
