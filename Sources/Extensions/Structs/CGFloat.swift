//
//  CGFloat.swift
//  Pods
//
//  Created by Martin Muller on 13.03.17.
//
//

import Foundation
import UIKit

extension CGFloat {
    public func changeBy(percentage: CGFloat) -> CGFloat {
        return (self / 100) * percentage
    }
}
