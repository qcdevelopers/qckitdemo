//
//  NSMutableAttributedString.swift
//  Pods
//
//  Created by Martin Muller on 13.03.17.
//
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    func bold(text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    func normal(text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}
