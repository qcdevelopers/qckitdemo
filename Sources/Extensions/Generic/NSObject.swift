//
//  GenericExt.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

extension NSObject {
    func with<T: NSObject>(_ modify: (_ s: T) -> ()) -> T {
        modify(self as! T)
        return self as! T
    }
}
