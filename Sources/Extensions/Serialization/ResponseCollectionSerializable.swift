//
//  ResponseCollectionSerializable.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import SwiftyJSON

public protocol ResponseCollectionSerializable {
    static func collection(_ json: JSON) -> [Self]
}
