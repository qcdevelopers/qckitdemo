//
//  Variable.swift
//  Alamofire
//
//  Created by Martin Muller on 14.01.18.
//

import Foundation

#if !RX_NO_MODULE
    import RxSwift
    import RxCocoa
#endif

extension Variable where Element == Void? {
    public func trigger() { self.value = Void() }
    
    public func deOptDriver() -> Driver<Void> {
        return self.asDriver().filter { (voidOpt) -> Bool in
            if (voidOpt != nil) { return true }
            else { return false }
            }.map { (voidOpt) -> Void in
                guard let void = voidOpt else { preconditionFailure("Void cannot be nil") }
                return void }
    }
}

extension Variable where Element == Int? {
    public func trigger(with value: Int) { self.value = value }
    
    public func deOptDriver() -> Driver<Int> {
        return self.asDriver().filter { (opt) -> Bool in
            if (opt != nil) { return true }
            else { return false }
            }.map { (opt) -> Int in
                guard let result = opt else { preconditionFailure("Void cannot be nil") }
                return result }
    }
}

extension Variable where Element == String? {
    public func trigger(with value: String) { self.value = value }
    
    public func deOptDriver() -> Driver<String> {
        return self.asDriver().filter { (opt) -> Bool in
            if (opt != nil) { return true }
            else { return false }
            }.map { (opt) -> String in
                guard let result = opt else { preconditionFailure("Void cannot be nil") }
                return result }
    }
}
