//
//  CLGeocoder+Rx.swift
//
//  Created by Daniel Tartaglia on 5/7/16.
//  Copyright © 2016 Daniel Tartaglia. MIT License.
//
//  see https://gist.github.com/dtartaglia/64bda2a32c18b8c28e1e22085a05df5a
//

//import CoreLocation
//import RxSwift
//
//public extension CLGeocoder {    
//    func rx_reverseGeocode(_ location: CLLocation) -> Observable<[CLPlacemark]> {
//        return Observable<[CLPlacemark]>.create { observer in
//            geocodeHandler(observer, geocode: curry2(self.reverseGeocodeLocation, location))
//            return Disposables.create { self.cancelGeocode() }
//        }
//    }
//    
//    func rx_geocodeAddressDictionary(_ addressDictionary: [AnyHashable: Any]) -> Observable<[CLPlacemark]> {
//        return Observable<[CLPlacemark]>.create { observer in
//            geocodeHandler(observer, geocode: curry2(self.geocodeAddressDictionary, addressDictionary))
//            return Disposables.create { self.cancelGeocode() }
//        }
//    }
//    
//    func rx_geocodeAddressString(_ addressString: String) -> Observable<[CLPlacemark]> {
//        return Observable<[CLPlacemark]>.create { observer in
//            geocodeHandler(observer, geocode: curry2(self.geocodeAddressString, addressString))
//            return Disposables.create { self.cancelGeocode() }
//        }
//    }
//    
//    func rx_geocodeAddressString(_ addressString: String, inRegion region: CLRegion?) -> Observable<[CLPlacemark]> {
//        return Observable<[CLPlacemark]>.create { observer in
//            geocodeHandler(observer, geocode: curry3(self.geocodeAddressString, addressString, region))
//            return Disposables.create { self.cancelGeocode() }
//        }
//    }
//}
//
//private func curry2<A, B, C>(_ f:  @escaping (A, B) -> C, _ a: A) -> (B) -> C {
//    return { b in f(a, b) }
//}
//
//private func curry3<A, B, C, D>(_ f: @escaping (A, B, C) -> D, _ a: A, _ b: B) -> (C) -> D {
//    return { c in f(a, b, c) }
//}
//
//private func geocodeHandler(_ observer: AnyObserver<[CLPlacemark]>, geocode: @escaping (CLGeocodeCompletionHandler) -> Void) {
//    let semaphore = DispatchSemaphore(value: 0)
//    waitForCompletionQueue.async {
//        geocode { placemarks, error in
//            semaphore.signal()
//            if let placemarks = placemarks {
//                observer.onNext(placemarks)
//                observer.onCompleted()
//            }
//            else if let error = error {
//                observer.onError(error)
//            }
//            else {
//                observer.onError(RxError.unknown)
//            }
//        }
//        let waitUntil = DispatchTime.now() + Double(Int64(30 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//        semaphore.wait(timeout: waitUntil)
//    }
//}
//
//private let waitForCompletionQueue = DispatchQueue(label: "WaitForGeocodeCompletionQueue", attributes: [])
