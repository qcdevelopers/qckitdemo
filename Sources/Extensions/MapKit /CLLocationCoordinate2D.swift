//
//  CLLocationCoordinate2D.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import MapKit

extension CLLocationCoordinate2D {
    public func toCLLocation() -> CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
}