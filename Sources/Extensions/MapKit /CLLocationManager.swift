//
//  CLLocationManagerExtensions.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import MapKit

extension CLLocationManager {
    public var latitude: Double { return location?.coordinate.latitude ?? 0 }
    public var longitude: Double { return location?.coordinate.longitude ?? 0 }
}