//
//  UICollectionViewExt.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

extension UICollectionView {
    public func scroll(to position: UICollectionViewScrollPosition, animated: Bool = true) {
        let section = position == .bottom ? numberOfSections - 1 : 0
        let row = position == .bottom ? numberOfItems(inSection: section) - 1 : 0
        scrollToItem(at: IndexPath(row: row, section: section), at: position, animated: animated)
    }
    
    public func dequeueCell<T: UICollectionViewCell>(_ id: String? = "Cell", indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: id!, for: indexPath) as? T
    }

    func animate(usingSpringWith duration: TimeInterval = 0.5, damping: CGFloat = 0.8) {
        self.reloadData()
        
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        
        for i in cells {
            let cell: UICollectionViewCell = i as UICollectionViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UICollectionViewCell = a as UICollectionViewCell
            UIView.animate(withDuration: duration, delay: 0.05 * Double(index), usingSpringWithDamping: damping, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
}
