//
//  UIBarButtonItem.swift
//  Pods
//
//  Created by Martin Muller on 15.07.17.
//
//

import Foundation

extension UIBarButtonItem {
    public func with(title: String) -> UIBarButtonItem {
        self.title = title
        return self
    }
}
