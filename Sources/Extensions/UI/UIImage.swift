//
//  UIImageExtensions.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    public func crop(_ bound : CGRect) -> UIImage {
        let scaledBounds : CGRect = CGRect(x: bound.origin.x * scale, y: bound.origin.y * scale, width: size.width * scale, height: bound.size.height * scale)
        let imageRef = self.cgImage!.cropping(to: scaledBounds)
        return UIImage(cgImage: imageRef!, scale: scale, orientation: UIImageOrientation.up)
    }
    
    /// Initializer for image filled with color
    public convenience init?(color: UIColor, size: CGSize, opaque: Bool) {
        UIGraphicsBeginImageContextWithOptions(size, opaque, 0)
        let path = UIBezierPath(rect: CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        color.setFill()
        path.fill()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!)
    }
    
    public func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        var newSize: CGSize
        if(widthRatio > heightRatio) { newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio) }
        else { newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio) }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
