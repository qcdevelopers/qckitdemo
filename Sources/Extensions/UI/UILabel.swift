//
//  UILabel.swift
//  Pods
//
//  Created by Martin Muller on 13.03.17.
//
//

import Foundation
import UIKit

extension UILabel{
    func addTextSpacing(_ spacing: CGFloat){
        let attributedString = NSMutableAttributedString(string: text!)
        attributedString.addAttribute(NSAttributedStringKey.kern, value: spacing, range: NSRange(location: 0, length: text!.count))
        attributedText = attributedString
    }
}
