//
//  UIFont.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

extension UIFont {
    public static func printAllFontNames() {
        for family in UIFont.familyNames {
            for font in UIFont.fontNames(forFamilyName: family ) {
                print("Font family: \(family), Font name: \(font)")
            }
        }
    }
}
