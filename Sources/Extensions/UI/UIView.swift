//
//  UIViewExtensions.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    public var size: CGSize {
        get { return frame.size }
        set { frame.size = newValue }
    }
    public var height: CGFloat {
        get { return size.height }
        set { size.height = newValue }
    }
    public var width: CGFloat {
        get { return size.width }
        set { size.width = newValue }
    }
    public var origin: CGPoint {
        get { return frame.origin }
        set { frame.origin = newValue }
    }
    public var x: CGFloat {
        get { return frame.origin.x }
        set { frame.origin = CGPoint(x: newValue, y: frame.origin.y) }
    }
    public var y: CGFloat {
        get { return frame.origin.y }
        set { frame.origin = CGPoint(x: frame.origin.x, y: newValue) }
    }
    
    public var maxHeight: CGFloat { return sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude)).height }
    public var maxWidth:  CGFloat { return sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: size.height)).width }
    
    public func roundCorners(_ radius: CGFloat) { layer.cornerRadius = radius }
    
    public func addSwipeGesture(_ dir: UISwipeGestureRecognizerDirection, action: Selector) {
        let r = UISwipeGestureRecognizer(target: self, action: action)
        r.direction = dir
        self.addGestureRecognizer(r)
    }
    
    public func round() {
        clipsToBounds = true
        layer.cornerRadius = frame.size.width / 2;
    }
    
    public func makeShadowAroundBorders() {
        self.layer.shadowOpacity = 0.55
        self.layer.shadowRadius = 5.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.5)
    }
    
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    public func addParallaxToView(amount: Int = 15) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        self.addMotionEffect(group)
    }
    
    public func pushTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = duration
        self.layer.add(animation, forKey: kCATransitionPush)
    }
}
