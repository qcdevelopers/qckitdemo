//
//  UINavigationBarExt.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    public static func makeTransparent() {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().backgroundColor = UIColor.clear
    }
    
    public func makeTransparent() {
        setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        shadowImage = UIImage()
        isTranslucent = true
        backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
    }
    
    public static func setTitleAttributes(_ color: UIColor? = UIColor.black, font: UIFont? = UIFont.systemFont(ofSize: 17)) {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: color!, NSAttributedStringKey.font: font!]
    }
    
    public func setTitleAttributes(_ color: UIColor? = UIColor.black, font: UIFont? = UIFont.systemFont(ofSize: 17)) {
        titleTextAttributes = [NSAttributedStringKey.foregroundColor: color!, NSAttributedStringKey.font: font!]
    }
    
    // MARK: Doesnt work
    public static func removeBorder() {
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    }
    
    public func remove() {
        shadowImage = UIImage()
        setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    }
}
