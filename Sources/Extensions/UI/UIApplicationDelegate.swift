//
//  UIApplicationDelegateExt.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

extension UIApplicationDelegate {
    public func changeRootViewController(to vc: UIViewController, animated: Bool = true) {
        if animated {
            let snapshot = self.window!!.snapshotView(afterScreenUpdates: true)
            vc.view.addSubview(snapshot!)
            
            window??.rootViewController = vc
            
            UIView.animate(withDuration: 0.3, animations: { () in
                snapshot?.layer.opacity = 0;
                snapshot?.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
                }, completion: { (value) in
                    snapshot?.removeFromSuperview();
            })
        } else {
            window??.rootViewController = vc
        }
    }
}
