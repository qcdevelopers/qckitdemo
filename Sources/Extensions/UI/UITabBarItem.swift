//
//  UITabBarItem.swift
//  Pods
//
//  Created by Martin Muller on 21.07.17.
//
//

import UIKit

public extension UITabBar {
    public func add(image: UIImage, frame: CGRect) {
        let imageViewCircle = image
        let tapbarCircle = UIImageView(frame: frame)
        
        tapbarCircle.contentMode = .scaleAspectFit
        tapbarCircle.image = imageViewCircle
        
        self.addSubview(tapbarCircle)
    }
}
