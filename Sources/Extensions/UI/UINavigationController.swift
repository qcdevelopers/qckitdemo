//
//  UINavigationController.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 07.08.16.
//
// see http://nshipster.com/associated-objects/
//

import Foundation
import UIKit

public extension UINavigationController {
    fileprivate struct AssociatedKeys {
        static var currentCompletioObjectHandle = "currentCompletioObjectHandle"
    }
    public typealias Completion = @convention(block) (UIViewController, Bool) -> ()
    
    var completionBlock: Completion? {
        get {
            let chBlock = unsafeBitCast(objc_getAssociatedObject(self, &AssociatedKeys.currentCompletioObjectHandle), to: Completion.self)
            return chBlock as Completion
        }
        set {
            if let newValue = newValue { // if not nil
                let newValueObj : AnyObject = unsafeBitCast(newValue, to: AnyObject.self)
                objc_setAssociatedObject(self, &AssociatedKeys.currentCompletioObjectHandle, newValueObj, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
            else {
                objc_setAssociatedObject(self, &AssociatedKeys.currentCompletioObjectHandle, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    /// Warning: When you use UINavigationControllerDelegate, please call 
    /// ensureCompletionBlockForViewController() in navigationController:didShowViewController
    public func popViewControllerAnimated(_ animated: Bool, completion: Completion?) {
        if let completion = completion {
            if (self.delegate == nil) {
                self.delegate = self
            }
            completionBlock = completion
            self.popViewController(animated: animated)
        } else {
            self.popViewController(animated: animated)
        }
    }
    
    /// Warning: When you use UINavigationControllerDelegate, please call
    /// ensureCompletionBlockForViewController() in navigationController:didShowViewController
    public func pushViewController(_ viewController: UIViewController, animated: Bool, completion: Completion?) {
        if let completion = completion {
            if (self.delegate == nil) {
                self.delegate = self
            }
            completionBlock = completion
            self.pushViewController(viewController, animated: animated)
        } else {
            self.pushViewController(viewController, animated: animated)
        }
    }
    
    public func ensureCompletionBlockForViewController(_ viewController: UIViewController, animated: Bool) {
        if let completion = completionBlock {
            completion(viewController, animated)
            completionBlock = nil
        }
    }
}

// MARK: UINavigationControllerDelegate
extension UINavigationController : UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if completionBlock != nil {
            self.ensureCompletionBlockForViewController(viewController, animated: animated)
            self.delegate = nil
        }
    }
}
