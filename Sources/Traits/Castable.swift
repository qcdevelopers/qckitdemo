//
//  Castable.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

public protocol Castable {
    func cast<T>() -> T?
}

extension Castable {
    public func cast<T>() -> T? { return self as? T }
}