//
//  CircularBuffer.swift
//
//  Copyright (c) 2014 Pelfunc, Inc. All rights reserved.
//

/// Allow iteration of CircularBuffer<T> in LIFO order
public struct CircularBufferGenerator<T> : IteratorProtocol {
    public mutating func next() -> T?
    {
        if remainingCount > 0 {
            let value = buffer[index%buffer.count]
            remainingCount -= 1
            index += 1
            return value
        }
        return nil
    }
    
    fileprivate init(_ ring: CircularBuffer<T>!) {
        buffer = ring.buffer
        remainingCount = ring.storedCount
        index = (ring.count-remainingCount) % ring.capacity
    }
    
    fileprivate let buffer: [T]
    fileprivate var index: Int
    fileprivate var remainingCount: Int
}

/// A ring keeps track of the last n objects where n is set
/// using capacity.  The ring can be iterated on in LIFO
/// order.
open class CircularBuffer<T> : Sequence {
    
    /// Where the buffer is stored.
    final fileprivate var buffer: [T] = []
    
    /// The total number of times add() has been called since
    /// init or the last reset.
    open fileprivate(set) var count: Int = 0
    
    /// The number of objects this ring can store.
    open var capacity: Int = 0
    
    /// The number of objects store in the ring.
    open var storedCount: Int {
        return Swift.min(count, capacity)
    }
    
    /// Create an empty ring with the specified capacity
    /// @requires capacity must be > 0
    public init(capacity: Int) {
        assert(capacity > 0)
        self.capacity = capacity
    }
    
    /// Add value to the ring.
    open func add(_ value: T) {
        if !isFull {
            buffer.append(value)
        }
        else {
            buffer[count % buffer.count] = value
        }
        count += 1
    }
    
    /// The value that will be removed when a new values is
    /// added to the ring.
    open var willRemoveValue: T? {
        if isFull {
            let index = (count-storedCount) % capacity
            return buffer[index]
        }
        return nil
    }
    
    /// Set the storedCount to zero.
    open func reset() {
        count = 0
        buffer = []
    }
    
    /// Return true if the ring is empty
    open var isEmpty: Bool {
        return count == 0
    }
    
    /// Return true if the ring is full and newly added values will bump out old ones.
    open var isFull: Bool {
        return count >= capacity
    }
    
    /// Allow iteration from oldest to newest value.
    open func makeIterator() -> CircularBufferGenerator<T> {
        return CircularBufferGenerator(self)
    }
}
