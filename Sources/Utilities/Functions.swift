//
//  Functions.swift
//  JuicymoKit Pod
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

// Native Frameworks
import Foundation
import UIKit

// Function for formating JSON in console logging in Moya
public func JuicymoJSONDebugRDF(_ data: Data) -> Data { // RDF = ResponseDataFormatter
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data, options: [])
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data //fallback to original data if it cant be serialized
    }
}

public func trace(_ identifier: String? = nil, file: String = #file, line: UInt = #line, function: String = #function) {
    if let identifier = identifier {
        print("[JuicymoKit] Trace \(identifier)")
    }
    else {
        let trimmedFile: String
        if let lastIndex = file.lastIndexOf("/") {
            trimmedFile = String(file[file.index(after: lastIndex) ..< file.endIndex])
        }
        else {
            trimmedFile = file
        }
        print("[JuicymoKit] Trace \(trimmedFile):\(line) \(function)")
    }
}

public func traceClass(_ clazz: String, identifier: String? = nil, file: String = #file, line: UInt = #line, function: String = #function) {
    if let identifier = identifier {
        print("[JuicymoKit] Trace \(clazz): \(identifier)")
    }
    else {
        let trimmedFile: String
        if let lastIndex = file.lastIndexOf("/") {
            trimmedFile = String(file[file.index(after: lastIndex) ..< file.endIndex])
        }
        else {
            trimmedFile = file
        }
        print("[JuicymoKit] Trace \(trimmedFile):\(line) \(clazz)::\(function)")
    }
}

// MARK: WTF (should be removed?)
public func delay(_ delay: Double, closure: @escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
