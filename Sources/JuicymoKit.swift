//
//  JuicymoKit.swift
//  JuicymoKit
//
//  Created by Martin Budínský and Tomáš Jukin on 31.05.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

// Native Frameworks
import Foundation

// Dependencies
import Swinject
import ObjectMapper



public typealias JuicymoDI = Container
public typealias JuicymoEntityObjectMapper = Mappable
public typealias JuicymoOnCallback = () -> (Void)

open class JuicymoKit {

}
