//
//  JuicymoModelP.swift
//  Pods
//
//  Created by Tomáš Jukin on 19.12.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import RxSwift
import RxCocoa

public protocol JuicymoModelP: JuicymoDIRegister, JuicymoDIProvider {
    /// Can be used to invoke model refresh from Rx
    var actionRefreshDefault: Driver<Void> { get set }
    
    /// Can be used to invoke model refresh from non-Rx code
    /// By default is driven by actionRefresh
    /// When a boolean value of true is passed, refresh is invoked, false is ignored
    /// Is se to true by default, so it is HOT
    var triggerRefreshDefault: Variable<Bool> { get set }
    
    // MARK: Public Methods (called from VM)
    func processInputBindings()
    func performRefreshDefault()
    func prepareOutputBindings()
}