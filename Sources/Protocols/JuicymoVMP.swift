//
//  JuicymoVMP.swift
//  Pods
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import RxSwift
import RxCocoa

public protocol JuicymoVMP: JuicymoDIProvider, JuicymoDIRegister {
    /// Is this VM active? (should is forward data)
    var active: Bool { get set }
    
    /// Can be used to invoke VM refresh from Rx
    var actionRefreshDefault: Driver<Void> { get set }
    
    /// Can be used to invoke VM refresh from non-Rx code
    /// By default is driven by actionRefresh
    /// When a boolean value of true is passed, refresh is invoked, false is ignored 
    /// Is se to true by default, so it is COLD
    var triggerRefreshDefault: Variable<Bool> { get set }
    
    // MARK: Public Methods (called from VC)
    func invalidate() // will cause refresh on next call to refreshIfNeeded()
    func refreshIfNeeded() // perform refresh when VM is invalidated
    func refresh() // perform refresh immediatelly
    func processInputBindings() // every VM needs to process input bindings from VC
    func prepareOutputBindings() // every VM needs to prepare output bindings for VC
    
    // MARK: Protected Methods
    func performRefresh() // every VM needs to be able to refresh itself
    func cleanUp() // every VM needs to know how it should be cleaned up on its VC dismiss
}