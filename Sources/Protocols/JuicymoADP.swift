//
//  JuicymoAD.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 01.07.16.
//
//

// Native Frameworks
import Foundation

/**
 Example usage:
 
 // MARK: Lifecycle
 func application(application: UIApplication,
      didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
         setupServices()
         setupGlobalAppearance()
         prepareWindow()
         setupRootVC()
         
         return true
 }
 
 */
public protocol JuicymoADP {
    func setupServices()
    func setupRootVC()    
    func setupGlobalAppearance()
    
    func prepareWindow()
}