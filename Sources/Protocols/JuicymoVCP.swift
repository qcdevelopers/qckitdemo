//
//  JuicymoVCP.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies

public protocol JuicymoVCP {
    var navbarProgressColor: UIColor? { get set }
    
    func connectToDIContainer(_ di: JuicymoDI)
    
    func bindRefreshToViewModel()
    func bindToViewModel(_ vm: JuicymoVMP)
}
