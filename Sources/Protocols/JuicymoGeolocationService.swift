//
//  JuicymoGeolocationService.swift
//  Pods
//
//  Created by Tomáš Jukin on 19.01.17.
//
//

// Native Frameworks
import Foundation
import CoreLocation

// Dependencies
import RxSwift
import RxCocoa

public protocol JuicymoGeolocationService {
    var authorized: Driver<Bool> { get }
    var location: Driver<CLLocationCoordinate2D> { get }
}
