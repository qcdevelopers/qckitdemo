//
//  JuicymoDIRegister.swift
//  Pods
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
//import Swinject

public protocol JuicymoDIRegister {
    func setupDI(_ di:JuicymoDI)
}
