//
//  JuicymoModelOwner.swift
//  Pods
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import Swinject

// Classes conforming to this protocol can provide Juicymo DI
public protocol JuicymoDIProvider {
    var di:JuicymoDI { get }
}