//
//  JuicymoFCP.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies

public protocol JuicymoFCP: JuicymoDIProvider {
    var ac: JuicymoACP { get }
    var baseFlow: UINavigationController? { get }
    
    func loadInitialVCinBaseFlow<T: UIViewController>() -> T
    func loadInitialVCinBaseFlow<T: UIViewController>(_ skipConfigApply: Bool) -> T
    func loadInitialVCinChildFlow<T: UIViewController>(_ flowName: String) -> T
    func loadInitialVCinChildFlow<T: UIViewController>(_ flowName: String, skipConfigApply: Bool) -> T
    
    func loadBaseFlowWithInitialVC() -> UINavigationController
    func loadChildFlowWithInitialVC(_ flowName: String) -> UINavigationController
    func loadChildFlowWithVC(_ name: String, flowName: String?) -> UINavigationController
    
    func loadInitialTabBarC() -> UITabBarController
    func loadInitialVC<T: UIViewController>() -> T
    func loadInitialVC<T: UIViewController>(_ skipConfigApply: Bool) -> T
    func loadVC<T: UIViewController>(_ name: String) -> T
    func loadVC<T: UIViewController>(_ name: String, skipConfigApply: Bool) -> T
    func loadVCinChildFlow<T: UIViewController>(_ name: String, flowName: String?) -> T
    func loadVCinChildFlow<T: UIViewController>(_ name: String, flowName: String?, skipConfigApply: Bool) -> T
    
    func getBaseFlow() -> UINavigationController
    
    func startNetworkActivityIndication()
    func stopNetworkActivityIndication()
    
    func applyConfigurationToVC(_ vcName: String, vc: UIViewController, presentingVC: UIViewController?, presentingFC: JuicymoFCP?)
    
    func baseDiveIn(_ vc: UIViewController, animated: Bool, completion: ((UIViewController, Bool) -> Void)?)
    func baseBackTo(_ animated: Bool, completion: ((UIViewController, Bool) -> Void)?)
    func baseOpen(_ vc: UIViewController, animated: Bool, transitionStyle: UIModalTransitionStyle, completion: (() -> Void)?)
    func baseClose(_ animated: Bool, completion: (() -> Void)?)
    
    func childDiveIn(_ vc: UIViewController, fromFlow: String, animated: Bool, completion: ((UIViewController, Bool) -> Void)?)
    func childBackTo(_ fromFlow: String, animated: Bool, completion: ((UIViewController, Bool) -> Void)?)
    func childOpen(_ vc: UIViewController, fromFlow: String, animated: Bool, transitionStyle: UIModalTransitionStyle, completion: (() -> Void)?)
    func childClose(_ fromFlow: String, animated: Bool, completion: (() -> Void)?)
    
    //flowXX methods
    func isLoggedIn() -> Bool
    func ensureLogin(_ onDone: () -> (Void))
    func openAuthenticationFlow(_ onDone: () -> (Void))
}
