//
//  JuicymoACP.swift
//  JuicymoKit
//
//  Created by Tomáš Jukin on 02.07.16.
//
//

// Native Frameworks
import Foundation

// Dependencies
import Swinject

public protocol JuicymoACP: JuicymoDIProvider, Castable {
    var rootFlow: JuicymoFCP? { get set }
    var appDelegate: UIApplicationDelegate? { get set }
    
    func createRootVC<T: JuicymoVCP>() -> T
    func createRootVCinNVC() -> UINavigationController
    func createRootVCasTabBar() -> UITabBarController
    func changeRootVCasTabBar(with rFlow: JuicymoFCP)
    func changeRootVCinNVC(with rFlow: JuicymoFCP)
    
    func setupRootFlow()
    func setupServices()
    
    func defineBindings()
    
    func onApplicationStart()
    
    func startNetworkActivityIndication()
    func stopNetworkActivityIndication()
}
