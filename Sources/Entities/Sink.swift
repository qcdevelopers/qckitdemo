//
//  Void.swift
//  UrateU
//
//  Created by Tomáš Jukin on 17.09.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation

import ObjectMapper

open class Sink: JuicymoEntityObjectMapper {
    open var id: Int?
    
    required public init?(map: Map) {}
    
    open func mapping(map: Map) {
        id <- map["id"]
    }
    
    open static func empty(_ data: [String: String] = [:]) -> Sink {
        let json = "{}"
        let jsonDictionary = Mapper<Sink>.parseJSONString(JSONString: json)
        let mapData: Map = Map(mappingType: .fromJSON, JSON: jsonDictionary! as! [String : Any], toObject: true)
        return Sink(map: mapData)!
    }
}
