//
//  NetworkIndicatorService.swift
//  JuicymoKit
//
//  Created by Martin Muller on 23.01.18.
//

import Foundation

import AlamofireNetworkActivityIndicator
import RxSwift
import RxCocoa

protocol NetworkIndicatorServiceP {
    var downloadState: Driver<Bool?> { get }
}

class NetworkIndicatorService: NetworkIndicatorServiceP {
    private(set) var downloadState: Driver<Bool?> = Driver.never()
    
    init() {
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.0
        downloadState = configureDownloadState()
    }
    
    private func configureDownloadState() -> Driver<Bool?> {
        return Observable.create { (observer) -> Disposable in
            NetworkActivityIndicatorManager.shared.networkActivityIndicatorVisibilityChanged = { visibility in observer.onNext(visibility) }
            return Disposables.create() { } }.asDriver(onErrorJustReturn: nil)
    }
}
