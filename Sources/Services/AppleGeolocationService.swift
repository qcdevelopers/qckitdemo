//
//  GeolocationService.swift
//  UrateU
//
//  Created by Tomáš Jukin on 02.07.16.
//  Copyright © 2016 Juicymo s.r.o. All rights reserved.
//

import Foundation
import CoreLocation

import RxSwift
import RxCocoa

public class AppleGeolocationService: JuicymoGeolocationService {
    private (set) public var authorized: Driver<Bool>
    private (set) public var location: Driver<CLLocationCoordinate2D>
    private (set) var addresses: Driver<[CLPlacemark]> = Driver.never()
    private (set) var address: Driver<CLPlacemark?> = Driver.never()
    
    public let locationManager = CLLocationManager()
    private let geocoder = CLGeocoder()
    
    public init() {
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        authorized = Observable.deferred { [weak locationManager] in
            let status = CLLocationManager.authorizationStatus()
            guard let locationManager = locationManager else {
                return Observable.just(status)
            }
            return locationManager
                .rx.didChangeAuthorizationStatus
                .startWith(status)
            }
            .asDriver(onErrorJustReturn: CLAuthorizationStatus.notDetermined)
            .map {
                switch $0 {
                case .authorizedAlways:
                    return true
                case .authorizedWhenInUse:
                    return true
                default:
                    return false
                }
        }
        
        location = locationManager.rx.didUpdateLocations
            .asDriver(onErrorJustReturn: [])
            .flatMap {
                return $0.last.map(Driver.just) ?? Driver.empty()
            }
            .map { $0.coordinate }
        
        addresses = configureReverseGeocodingAll(location: location)
        
        address = configureReverseGeocodingFirst(location: location)
        
        locationManager.requestAlwaysAuthorization()
//        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    public func configureReverseGeocodingAll(location: Driver<CLLocationCoordinate2D>) -> Driver<[CLPlacemark]> {
        let localGeocoder = self.geocoder
        
        return location
            .distinctUntilChanged { (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool in
                return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude)
            }
            .map { (location: CLLocationCoordinate2D) -> Driver<[CLPlacemark]> in
                Observable.create { (observer) -> Disposable in
                    localGeocoder.reverseGeocodeLocation(location.toCLLocation(), completionHandler: { (placemark, error) in
                        if error != nil {
                            observer.on(.error(error!))
                        } else {
                            if placemark != nil {
                                observer.on(.next(placemark!))
                                observer.on(.completed)
                            } else {
                                observer.on(.error(error!))
                            }
                        }
                    })
                    
                    return Disposables.create() {
                        // noop
                    }
                    }.asDriver(onErrorJustReturn: [])
            }
            .switchLatest()
    }
    
    public func configureReverseGeocodingFirst(location: Driver<CLLocationCoordinate2D>) -> Driver<CLPlacemark?> {
        let localGeocoder = self.geocoder
        
        return location
            .distinctUntilChanged { (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool in
                return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude)
            }
            .map { (location: CLLocationCoordinate2D) -> Driver<CLPlacemark?> in
                Observable.create { (observer) -> Disposable in
                    localGeocoder.reverseGeocodeLocation(location.toCLLocation(), completionHandler: { (placemark, error) in
                        if error != nil {
                            observer.on(.error(error!))
                        } else {
                            if placemark != nil {
                                if placemark!.last != nil {
                                    observer.on(.next(placemark!.first))
                                } else {
                                    observer.on(.completed)
                                }
                            } else {
                                observer.on(.error(error!))
                            }
                        }
                    })
                    
                    return Disposables.create() {
                        // noop
                    }
                    }.asDriver(onErrorJustReturn: nil)
            }
            .switchLatest()
    }
}
