//
//  ReachabilityService.swift
//  Alamofire
//
//  Created by Martin Muller on 23.01.18.
//

import Foundation

import Alamofire
import RxSwift
import RxCocoa

protocol ReachabilityServiceP {
    var connectionState: Driver<Bool?> { get }
}

class ReachabilityService: ReachabilityServiceP {
    private let networkReachability = NetworkReachabilityManager()!
    private(set) var connectionState: Driver<Bool?> = Driver.never()
    
    public init() {
        self.networkReachability.startListening()
        self.connectionState = configureConnectionState()
    }
    
    private func configureConnectionState() -> Driver<Bool?> {
        return Observable.create { (observer) -> Disposable in
            self.networkReachability.listener = { flag in
                if flag == .notReachable { observer.onNext(false) }
                else if flag == .reachable(.ethernetOrWiFi) || flag == .reachable(.wwan) { observer.onNext(true) } }
            
            return Disposables.create() { }
            }.asDriver(onErrorJustReturn: nil)
            .startWith(networkReachability.isReachable)
    }
}
